
var map;
var lat;
var lng;
var directionsService = new google.maps.DirectionsService();
var markerArray = [];
var stepDisplay;
function initialize() {
  var mapOptions = {
    zoom: 6
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
    directionsDisplay = new google.maps.DirectionsRenderer();
    directionsDisplay.setMap(map);
    
  // Try HTML5 geolocation
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);
      lat=position.coords.latitude;
      lng=position.coords.longitude;
      calcRoute();
     stepDisplay = new google.maps.InfoWindow();
    }, function() {
      handleNoGeolocation(true);
    });
  } else { 
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }
}
function calcRoute() {
  var start =lat+","+lng;
  //var end ="valdosta ga";

  var request = {
      origin:start,
      destination:end,
      travelMode: google.maps.TravelMode.DRIVING
  };
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
      showSteps(response);
    }
  });
}
function attachInstructionText(marker, text) {
  google.maps.event.addListener(marker, 'click', function() {
    // Open an info window when the marker is clicked on,
    // containing the text of the step.
    stepDisplay.setContent(text);
    stepDisplay.open(map, marker);
  });
}
function showSteps(directionResult) {
  // For each step, place a marker, and add the text to the marker's
  // info window. Also attach the marker to an array so we
  // can keep track of it and remove it when calculating new
  // routes.
  var myRoute = directionResult.routes[0].legs[0];
  var str="";
  for (var i = 0; i < myRoute.steps.length; i++) {
    var marker = new google.maps.Marker({
      position: myRoute.steps[i].start_location,
      map: map,
        icon:"http://maps.gstatic.com/intl/en_ALL/mapfiles/markers2/measle.png"
    });
    attachInstructionText(marker, myRoute.steps[i].instructions);
    console.log(myRoute.steps[i]);
    str=str+"<p>"+myRoute.steps[i].distance.text+"  "+myRoute.steps[i].instructions+"</p>";
    markerArray[i] = marker;
  }

  document.getElementById("directions").innerHTML =str;
  window.console.log(str);
}

function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }

  var options = {
    map: map,
    position: new google.maps.LatLng(60, 105),
    content: content
  };

  var infowindow = new google.maps.InfoWindow(options);
  map.setCenter(options.position);
}


//http://maps.googleapis.com/maps/api/directions/json?origin=29%20iniss%20rd%20rifton%20ga&destination=%22lenox%20ga


google.maps.event.addDomListener(window, 'load', initialize);