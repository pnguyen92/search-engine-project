jQuery(function($) {
	"use strict";
	var total;
	var gmarkers = [];
	var map, xml, customIcons, bounds,
		infoWindow = new google.maps.InfoWindow(),
		markerGroups = {},
		groupIcons = {},
		lastIcon = 0,
		baseURL = "http://chart.apis.google.com/chart?cht=mm&chs=20x20&ext=.png&chco=ffffff,",
		sixZeros = ",000000";
	/** /
	var iconRed			= (new StyledIcon(StyledIconTypes.MARKER,{color:"FF0000"})).icon,
		iconBlue		= (new StyledIcon(StyledIconTypes.MARKER,{color:"0000FF"})).icon,
		iconBlack		= (new StyledIcon(StyledIconTypes.MARKER,{color:"000000"})).icon,
		iconYellow		= (new StyledIcon(StyledIconTypes.MARKER,{color:"FFFF00"})).icon,
		iconBrown		= (new StyledIcon(StyledIconTypes.MARKER,{color:"663300"})).icon,
		iconPurple		= (new StyledIcon(StyledIconTypes.MARKER,{color:"FF00FF"})).icon,
		iconGreen		= (new StyledIcon(StyledIconTypes.MARKER,{color:"00FF00"})).icon,
		iconGray		= (new StyledIcon(StyledIconTypes.MARKER,{color:"808080"})).icon,
		iconWhite		= (new StyledIcon(StyledIconTypes.MARKER,{color:"FFFFFF"})).icon,
		iconDarkRed		= (new StyledIcon(StyledIconTypes.MARKER,{color:"800000"})).icon,
		iconDarkGreen	= (new StyledIcon(StyledIconTypes.MARKER,{color:"008000"})).icon,
		iconDarKYellow	= (new StyledIcon(StyledIconTypes.MARKER,{color:"808000"})).icon,
		iconDarkBlue	= (new StyledIcon(StyledIconTypes.MARKER,{color:"000080"})).icon,
		iconDarkMagenta	= (new StyledIcon(StyledIconTypes.MARKER,{color:"800080"})).icon,
		iconDarkCyan	= (new StyledIcon(StyledIconTypes.MARKER,{color:"008080"})).icon,
		iconLightGray	= (new StyledIcon(StyledIconTypes.MARKER,{color:"C0C0C0"})).icon,
		iconLightGreen	= (new StyledIcon(StyledIconTypes.MARKER,{color:"C0DCC0"})).icon,
		iconLightBlue	= (new StyledIcon(StyledIconTypes.MARKER,{color:"A6CAF0"})).icon;
	/** /
	var iconRed			= baseURL+"FF0000"+sixZeros,
		iconBlue		= baseURL+"0000FF"+sixZeros,
		iconBlack		= baseURL+"000000"+sixZeros,
		iconYellow		= baseURL+"FFFF00"+sixZeros,
		iconBrown		= baseURL+"663300"+sixZeros,
		iconPurple		= baseURL+"FF00FF"+sixZeros,
		iconGreen		= baseURL+"00FF00"+sixZeros,
		iconGray		= baseURL+"808080"+sixZeros,
		iconWhite		= baseURL+"FFFFFF"+sixZeros,
		iconDarkRed		= baseURL+"800000"+sixZeros,
		iconDarkGreen	= baseURL+"008000"+sixZeros,
		iconDarKYellow	= baseURL+"808000"+sixZeros,
		iconDarkBlue	= baseURL+"000080"+sixZeros,
		iconDarkMagenta	= baseURL+"800080"+sixZeros,
		iconDarkCyan	= baseURL+"008080"+sixZeros,
		iconLightGray	= baseURL+"C0C0C0"+sixZeros,
		iconLightGreen	= baseURL+"C0DCC0"+sixZeros,
		iconLightBlue	= baseURL+"A6CAF0"+sixZeros;
	/**/
	customIcons = [ // 18 colors
		baseURL+"FF0000"+sixZeros,
		baseURL+"0000FF"+sixZeros,
		baseURL+"000000"+sixZeros,
		baseURL+"FFFF00"+sixZeros,
		baseURL+"663300"+sixZeros,
		baseURL+"FF00FF"+sixZeros,
		baseURL+"00FF00"+sixZeros,
		baseURL+"808080"+sixZeros,
		baseURL+"FFFFFF"+sixZeros,
		baseURL+"800000"+sixZeros,
		baseURL+"008000"+sixZeros,
		baseURL+"808000"+sixZeros,
		baseURL+"000080"+sixZeros,
		baseURL+"800080"+sixZeros,
		baseURL+"008080"+sixZeros,
		baseURL+"C0C0C0"+sixZeros,
		baseURL+"C0DCC0"+sixZeros,
		baseURL+"A6CAF0"+sixZeros
	];

	window.onload = function() {
			map = new google.maps.Map($("#map")[0], {
				center: new google.maps.LatLng(39.50,-98.35),
				zoom: 10,
				mapTypeId: google.maps.MapTypeId.SATELLITE,
				overviewMapControl: true,
				overviewMapControlOptions: { opened: true }
			});
		bounds = new google.maps.LatLngBounds();

		
		map.controls[google.maps.ControlPosition.TOP_LEFT].push( $("#logo")[0] );

		map.enableKeyDragZoom({
			visualEnabled: true,
			visualPosition: google.maps.ControlPosition.LEFT,
			visualPositionOffset: new google.maps.Size(35, 0),
			visualPositionIndex: null,
			visualSprite: "http://maps.gstatic.com/mapfiles/ftr/controls/dragzoom_btn.png",
			visualSize: new google.maps.Size(20, 20),
			visualTips: {
				off: "Turn on box zooming",
				on: "Turn off box zooming"
			}

		});
		setpoints(passvars);
		function setpoints(array){
		  	for (var i = 1; i < array.length; i++) {
               
                var bid = array[i]['business_id'],
					lat = array[i]['lat'],
					lng = array[i]['lng'],
					bname = array[i]['bname'],
					baddress = array[i]['baddress'],
					point = new google.maps.LatLng( parseFloat(lat), parseFloat(lng) );
				bounds.extend(point);
				var marker = createMarker( point, bid, bname,baddress);
				marker.setMap( map );
				gmarkers.push(marker);

            }
				map.fitBounds(bounds);
				//map.setCenter(bounds.getCenter());

				//google.maps.event.addListenerOnce(map, 'bounds_changed', function(){(map.getZoom() > 4) && map.setZoom(4); });
					map.setZoom(10);
	}
	};
	

	function createMarker( point, objid,bname,baddress) {
		//if ( !groupIcons[objid] ) {
		//	groupIcons[objid] = customIcons[ lastIcon++ % customIcons.length ];
		//}\

		var marker = new google.maps.Marker({
			position: point,
			id:objid,
			icon:customIcons[ lastIcon % customIcons.length ]// groupIcons[objid]
		});
	
			google.maps.event.addListener( marker, "click", function( e ) {
			var html = "<div style=\"width: 300px; font-size:12px; padding: 5px; line-height: 1.6em; \">"+
						"<a href=\"/review.php?id="+objid+"\" target=\"_blank\">"
						+bname+"</a><br/><a href=\"/directions.php?id="+objid+"\" target=\"_blank\">"
						+baddress+"</a><\/div>";
			infoWindow.setContent( html );
			infoWindow.open( map, marker );
		});

		if (objid in markerGroups ) {
			markerGroups[objid ].push( marker );
		} else {
			markerGroups[objid ] = [ marker ];
		}

		return marker;
	}

	$(document).ready(function(){
	    $("#fixed").affix({

	        offset: { 
	            top: 100
	            //,bottom:0
	     	}

	    });
	    var width = $('.results').width();
	    $('#map').width(width+20);
	    $('#map').height($(window).height()-250);
	    $('#fixed').css('margin-left',(width+50)+'px');
	});
	$(window).bind('resize', function() {
		var width = $('.results').width();
        $('#map').width(width+20);
        $('#map').height($(window).height()-250);
	    $('#fixed').css('margin-left',(width+50)+'px');
	    
    });
    /*$(document).on('click','.clickable',function(){
    	window.console.log("scroll");
     	var id= $(this).attr("data-id");
     	var target=$('#'+id);
     	
    	$('html,body').animate({
                 scrollTop: target.offset().top
            }, 500);
    	 setTimeout(function(){ 
				  	target.addClass("bg-success");
				  }, 500);
    	 setTimeout(function(){ 
				  	target.removeClass("bg-success");
				  }, 4000); 

    });*/
  $(".business").hover(function() {         
        var id=$(this).attr("id");
       // console.log("enter "+id);
        	markerGroups[id][0].setIcon(customIcons[ lastIcon+1 % customIcons.length ]);
        	
    }, function() {         
         var id=$(this).attr("id")    ;
       // console.log("exit "+id); 
        markerGroups[id][0].setIcon(customIcons[ lastIcon % customIcons.length ]);    
    }); 
  $(".business").on("click",function() {  
	    var id=$(this).attr("id");
	    map.setCenter(markerGroups[id][0].position);       
        google.maps.event.trigger(markerGroups[id][0], 'click');
    });
	$(".latlng").on('click',function(e){
		e.preventDefault();
			var newpt = new google.maps.LatLng($(this).attr("data-lat"),$(this).attr("data-lng"));
			for (var i=0; i<gmarkers.length;i++) {
				
		    if (gmarkers[i].id==$(this).attr("data-id")){
		    	gmarkers[i].setIcon( customIcons[ lastIcon+5 % customIcons.length] );
		    	var p = i;
		    	 setTimeout(function(){ 
				  	gmarkers[p].setIcon(customIcons[ lastIcon-1 ]);
				  }, 4000); 
		    }
		    
		   }

			map.panTo(newpt);
	});
	var oddclick = false;
	$('#submit').on('click',function(){
		
		$('#filter').toggle('slow');
		if($(window).width() > 992){
		$( "#fixed" ).animate({
			"top": oddclick?"350px":"50px"
			}, "slow" );
			oddclick =!oddclick;
		}
		
		
		//$('#fixed').toggleClass("fixeddown");

	});
	 $('input[type=radio][name=rating]').change(function() {
       window.location.href= "/index.php?rating="+this.value+"&what="+$("#what").val()+"&where="+ $("#where").val();
    });


});