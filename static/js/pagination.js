 //pagination
 	function refresh(page){
		var str=window.location.pathname+"?"+getParameterByName("rating")+getParameterByName("where")+
		getParameterByName("what")+"&page="+page+getParameterByName("id");
		window.location.href=str;
	}
	function pageleft(page){
		refresh(page-1);
	}
	function pageright(page){
		refresh(page+1);
	}
	function getParameterByName(name) {
		//params for business search are are rating,what,where,page
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    //return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));original
    return results === null ? "" : "&"+name+"="+decodeURIComponent(results[1].replace(/\+/g, " "));
}