<?php 
include_once '/includes/connections.php';
$limit = 10;
$page=empty($_GET["page"])?1:$_GET["page"];

$offset = (($page!=1?($page-1)*$limit:0));// this is my erro if page is one we are skipping to page 10
/* this method seems to work http://www.petefreitag.com/item/451.cfm
we need to use limit and offset to find our results*/
//highlight marker on hover of element
if(empty($_GET['id'])){
    $sql ="SELECT count(*) as count
    FROM business  
     WHERE open=1 "
     .(isset($_GET["what"])?'AND (lower(name) like "%'.$_GET["what"].'%" or lower(type) like "%'.$_GET["what"].'%"
         or business_id in (Select business_id from categories where category like "%'.$_GET["what"].'%"))
        ':'')
     .(isset($_GET["where"])?'AND( lower(full_address) like "%'.$_GET["where"].'%"
        or business_id in (Select business_id from neighborhoods where neighborhood like "%'.$_GET["where"].'%"))':'')
     .(isset($_GET['rating'])?' AND stars >= '.$_GET['rating']:'');
     //echo $sql.'<br/>';
    $result =mysqli_query($con,$sql);
     if (!$result) {
       die(mysqli_error($con));
    }
    //echo $sql;
    $row = $result->fetch_array();
    $resultcount= $row['count'];
   //echo $resultcount.'<br/>';
    //need to get the row count for pagination
    $query ="SELECT *
    FROM business  
     WHERE open=1 "
     .(isset($_GET["what"])?'AND (lower(name) like "%'.$_GET["what"].'%" or lower(type) like "%'.$_GET["what"].'%"
         or business_id in (Select business_id from categories where category like "%'.$_GET["what"].'%"))
        ':'')
     .(isset($_GET["where"])?'AND( lower(full_address) like "%'.$_GET["where"].'%"
        or business_id in (Select business_id from neighborhoods where neighborhood like "%'.$_GET["where"].'%"))':'')
     .(isset($_GET['rating'])?' AND stars = '.$_GET['rating']:'').
     ' order by stars desc LIMIT '. $limit.' offset '. $offset;
    //echo $query;
}else{
     $query ="SELECT *
    FROM business  
     WHERE open=1  AND business_id='".$_GET["id"]."'";
     //echo $query;
}
$result =mysqli_query($con,$query);
 if (!$result) {
   die(mysqli_error($con));
}
$class="alt";
$current=$offset+1;//check this

while ($row = mysqli_fetch_array($result)) {
	$str='';
	$class=($class=='alt')?"":"alt";
    echo'<div class="row business '.$class.'"  id="'.$row['business_id'].'">';
    echo'<div class="col-xs-12">';
    echo'<div class="col-xs-6"><h4>';
    $rating= explode(".",$row['stars']);
    $star=($rating[1]== 5)?'stars_'.$rating[0].'_half': 'stars_'.$rating[0];
    //beginning of patch
    /* business review count is off because some users didnt get entered running a global update query 
    SELECT review.bid ,count(*) as count FROM review inner join user on user.uid=review.uid
    where bid='hkLODtco5ITL3DjZignPSA' to correct this problem this patch is a temporary patch incase the update does not finish.*/
    $sql="SELECT count(*) as count FROM review inner join user on user.uid=review.uid
        WHERE bid = '".$row["business_id"]."'";
    $rc =mysqli_query($con,$sql);
    while ($row1 = mysqli_fetch_array($rc)) {
        $correctcount= $row1['count'];
    }
    //end of patch change $correctcount to $row['review_count'] on line 68
    echo $current.".  ".$row['name']
    .'</h4><p><div class="rating-large"><i class="star-img '.$star.'"></i></div><span>   '.$correctcount.' Reviews</span><br/>';
    /*$sql="SELECT value FROM attributes 
 		WHERE business_id = '".$row["business_id"]."AND attribute = 'Price Range'
 		LIMIT 1";
 	$price=mysqli_query($con,$sql);
 	if (!$price) {
    printf("Errormessage: %s\n", mysqli_error($link));
}
 	while ($row1 = mysqli_fetch_array($price)) {
 		$str= $row1['value'];
 	}
 	echo $str;*/
    $sql="SELECT * FROM categories  
 		WHERE business_id = '".$row["business_id"]."'
 		LIMIT 20";
 	$cat =mysqli_query($con,$sql);
 	while ($row1 = mysqli_fetch_array($cat)) {
 		$str= $row1['category'].',';
 	}
    echo '</p></div>';
    echo'<div class="col-xs-6"><p>';
    //get neighboorhoods
    $sql="SELECT * FROM neighborhoods  
 		WHERE business_id = '".$row["business_id"]."'
 		LIMIT 20";
 	$neighbor =mysqli_query($con,$sql);
 	while ($row2 = mysqli_fetch_array($neighbor)) {
 		$str= $row2['neighborhood'].',';
 	}
 	echo substr($str, 0, -1);
 	echo '<br/>'.$row['full_address'];
    echo '</p></div>';
    //echo $row['full_address'];
    //echo $row['open'];
    //echo $row['city'];
    echo '</div>
    		</div>';
   	$current++;
    if(!empty($thearray) && is_array($thearray)){
	   	$temp= array();
	   	$temp['business_id']=$row['business_id'];
        $temp['id']=$row['business_id'];
	   	$temp['lat']=$row['latitude'];
	   	$temp['lng']=$row['longitude'];
        $temp['bname']=$row['name'];
        $temp['baddress']=$row['full_address'];
	   	$thearray[]=$temp;
        //echo $thearray;
    }else{
        $thearray=$row['full_address'];
    }
}
?>
