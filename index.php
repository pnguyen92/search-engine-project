
<?php
$scripts="//maps.googleapis.com/maps/api/js?sensor=false,/static/js/terramap.js,/static/js/StyledMarker.js,/static/js/keydragzoom_packed.js,/static/js/action.js,/static/js/pagination.js";
$stylesheets="/static/css/stars.css,/static/css/affixmap.css";
include '/includes/header.php';
$thearray=array(array());
$checked =isset($_GET["rating"])?$_GET["rating"]:"";
$class=!empty($_GET)?"":"hide";
   
?>
<div class="row" id="thetop">
  <div class="col-xs-12 padding-bottom">
    <button type="submit" id="submit" value="submit" class="btn btn-default">
      Hide Filters
    </button>
  </div>
</div>
<div class="row well" id="filter">
  
  <div class="col-xs-12 border-top" >
    <div class="col-xs-12 col-sm-2">
      <h5><strong>
        Ratings
      </strong></h5>
      <div class="radio">
        <label>
          <input type="radio" <?php echo ($checked==5?"checked":"");?> name="rating"  value="5">
          <div class=" rating"><i class="star-img stars_5"></i></div>
        </label>
      </div>
      <div class="radio">
        <label>
          <input type="radio" <?php echo ($checked==4?"checked":"");?> name="rating"  value="4">
          <div class=" rating"><i class="star-img stars_4"></i></div>
        </label>
      </div>
      <div class="radio">
        <label>
          <input type="radio" <?php echo ($checked==3?"checked":"");?>  name="rating" value="3">
          <div class=" rating"><i class="star-img stars_3"></i></div>
        </label>
      </div>
      <div class="radio">
        <label>
          <input type="radio" <?php echo ($checked==1?"checked":"");?> name="rating"  value="2">
          <div class=" rating"><i class="star-img stars_2"></i></div>
        </label>
      </div>
      <div class="radio">
        <label>
          <input type="radio" <?php echo ($checked==1?"checked":"");?> name="rating"  value="1">
         <div class=" rating"><i class="star-img stars_1"></i></div>
        </label>
      </div>
    </div>
    <br/>
    
  </div>
</div>
</div>
<div class="row <?php echo $class;?>">
  <div id="fixed" class="fixeddown col-xs-12  col-md-6" >
    <a href="#thetop" CLASS="map">Back to Top</a>
    <div id="map" class="map" >
    </div>
  </div>
<div class="col-xs-12  col-md-6  ">
  <div class="col-xs-12 results  y-padding line">
       <?php 
       if (!empty($_GET)){
        include '/libs/modules/business.php';
         include '/includes/pagination.php';
       }
       else{
        echo "<p> please enter some search terms.</p>";
       }
      ?>
  </div>
</div>
 
    </div>
    </div>

    <?php 
    echo'<script> var passvars='.json_encode($thearray).';</script>';
    include '/includes/footer.php';
    ?>