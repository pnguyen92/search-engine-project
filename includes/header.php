 <!DOCTYPE HTML>
 <html lang="en">
    <head>
    	<title>Endeavor</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <script src="https://code.jquery.com/jquery.js"></script>
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="/static/bootstrap/css/bootstrap-xl.css">
        <!--<link rel="stylesheet" href="/static/css/main.css">-->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<?php 
			include '/includes/errors.php';
			if (isset( $scripts)) {
				$scriptarr = explode(',', $scripts);
				if(is_array($scriptarr)){
			    	foreach ($scriptarr as $item) {
			    		echo '<script src="'.$item.'" type="text/javascript"></script>';
			   		 }
			   	}else{
			   		echo '<script src="'.$scripts.'" type="text/javascript"></script>';
			   	}
		   	}
		   	if (isset( $stylesheets)) {
		   		 $stylesheetarr=explode(',', $stylesheets);
		   		if(is_array($stylesheetarr)){
			   		 foreach ($stylesheetarr as $item) {
			    		echo '<link rel="stylesheet" href="'.$item.'"/>';
			   	}
			   	}else{
			   		echo '<link rel="stylesheet" href="'.$stylesheets.'"/>';
			   	}
		   	}
	    ?>
	</head>
	<body>
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
    		<!-- Brand and toggle get grouped for better mobile display -->
    			<div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
      				<div class="navbar-brand" >Endeavor</div>
   					</div>
    				<!-- Collect the nav links, forms, and other content for toggling -->
      				<div class="collapse navbar-collapse">			
			          <ul class="nav navbar-nav">
			            <li class="#class#"><a href="/index.php">Home</a></li>
			            
			          </ul>
			          <form action="/index.php" class="navbar-form navbar-left  pull right" method="get" >
					        <div class="form-group">
					        	<?php 
					        	echo
					          '<input id="what" name="what" type="text" value="'.(isset($_GET["what"])?$_GET["what"]:'').'" class="form-control" placeholder="Find">
					        </div>
					        <div class="form-group">
					          <input id="where" name="where" value="'.(isset($_GET["where"])?$_GET["where"]:'').'" type="text" class="form-control" placeholder="Near">
					        </div>';
					        ?>
				        	<button type="submit" class="btn btn-default">Submit</button>
				      	</form>
    			</div><!-- /.navbar-collapse -->
  			</div><!-- /.container-fluid -->
		</nav>
		<div class="container">
